FROM aquasec/trivy:0.20.1

#Scan Filesystem for Vulnerabilities and Misconfigurations
ENTRYPOINT ["trivy","fs","--security-checks","vuln,config","."]
